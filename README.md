## Microblog in flask mini project

VER=0.8

A very small project of a personal microblog in Python3 flask just for practice.

Basically everything based on [Miguel Grinberg's excellent Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world)


How to run, on Ubuntu based systems:


`sudo apt install python3-pip python3-flask python3-virtualenv`

`git clone repo; cd microblog-in-flask`

`pip3 install -r requeriments.txt`

`. start.sh`


TO-DO: Create a CI pipeline for app development.
